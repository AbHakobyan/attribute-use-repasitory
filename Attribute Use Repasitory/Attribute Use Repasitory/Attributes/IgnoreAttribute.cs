﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory.Attributes
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple = false)]
    class IgnoreAttribute : Attribute
    {
    }
}
