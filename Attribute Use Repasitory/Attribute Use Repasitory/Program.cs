﻿using Attribute_Use_Repasitory.Models;
using Attribute_Use_Repasitory.Repazitory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = CreateStudents(10).ToList();
            var list = new Students_Repasitory();
            list.AddRange(student);
            list.SaveChanges();
            IEnumerable<Students> students = list.AsEnumerable();
            List<Students> studentss = students.ToList();
        }

        static IEnumerable<Students> CreateStudents(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Students
                {
                    Name = $"A{i + 1}",
                    Surename = $"A{i + 1}yan",
                    Age = (byte)rnd.Next(20, 35),
                    University = (University)rnd.Next(0, 4),
                    City = (City)rnd.Next(0, 8)
                };
            }
        }

        static IEnumerable<Teachers> CreateTeachers(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Teachers
                {
                    Name = $"T{i + 1}",
                    Surename = $"T{i + 1}yan",
                    Age = (byte)rnd.Next(20, 35),
                    University = (University)rnd.Next(0, 4),
                    City = (City)rnd.Next(0, 8)
                };
            }
        }

        static IEnumerable<Parents> CreateParents(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Parents
                {
                    Name = $"P{i + 1}",
                    Surename = $"P{i + 1}yan",
                    Age = (byte)rnd.Next(20, 35),
                    City = (City)rnd.Next(0, 8)
                };
            }
        }
    }

}

