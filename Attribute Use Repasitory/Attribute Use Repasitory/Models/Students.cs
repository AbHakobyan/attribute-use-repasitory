﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Attribute_Use_Repasitory;
using Attribute_Use_Repasitory.Attributes;

namespace Attribute_Use_Repasitory.Models
{
    class Students
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }
        public University University { get; set; }
        public City City { get; set; }

        [Ignore]
        public string Information => $"{Name} {Surename}";

        public override string ToString() => Information;
    }
}
