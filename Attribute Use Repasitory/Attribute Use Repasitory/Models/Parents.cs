﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory.Models
{
    class Parents
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }
        public City City { get; set; }

        public string Information => $"{Name} {Surename} {Age} {City}";

        public override string ToString() => Information;
    }
}
