﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory.Models
{
    class Teachers
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }
        public University University { get; set; }
        public City City { get; set; }

        public string Information => $"{Name} {Surename} {Age} {University} {City}";

        public override string ToString() => Information;
    }
}
