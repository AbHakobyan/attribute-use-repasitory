﻿using Attribute_Use_Repasitory.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory.Extension
{
    static class Ignore_Extension
    {
        public static bool IsIgnored(this PropertyInfo member)
        {
            return member.GetCustomAttribute<IgnoreAttribute>() != null; 
        }
    }
}
