﻿using Attribute_Use_Repasitory.Extension;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory.Repazitory
{
    abstract class Base_Repasitory<T> : IBase_Repasitory<T> where T : class, new()
    {
        public abstract string Path { get; }

        public Base_Repasitory() => _list = AsEnumerable().ToList();

        List<T> _list;

        public void Add(T model)
        {
            _list.Add(model);
        }

        public void AddRange(IEnumerable<T> model)
        {
            foreach (var item in model)
            {
                _list.Add(item);
            }
        }

        public IEnumerable<T> AsEnumerable()
        {
            if (!File.Exists(Path))
            {
                File.Create(Path).Dispose();
            }

            T model = null;
            StreamReader reader = new StreamReader(Path, Encoding.Default);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        model = new T();
                        break;
                    case "}":
                        yield return model;
                        break;
                    default:
                        ReadFill(line, model);
                        break;
                }
            }

            reader.Dispose();
        }

        public void Insert(int index, T model)
        {
            _list.Insert(index, model);
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(Path,true,Encoding.Default);
            foreach (var item in _list)
            {
                writer.WriteLine("{");
                WriteMOdel(writer, item);
                writer.WriteLine("}");
                count++;
            }
            _list.Clear();
            writer.Dispose();
            return count;
        }

        private void WriteMOdel(StreamWriter writer, T model)
        {
            var pis = model.GetType().GetProperties();
            foreach (var pi in pis)
            {
                if (pi.IsIgnored())
                {
                    continue;
                }

                object valuess = pi.GetValue(model);

                if (pi.PropertyType.IsEnum)
                {
                    writer.WriteLine($"{pi.Name}:{Convert.ToString(valuess)}");
                }
                else
                {
                    writer.WriteLine($"{pi.Name}:{valuess}");
                }
            }
        }

        private void OnReadFill(string[] data, T model)
        {
            string propName = data[0];
            var pis = model.GetType().GetProperty(propName, BindingFlags.Instance | BindingFlags.Public);
            if (pis != null)
            {
                if (pis.PropertyType.IsEnum)
                {
                    object values = Enum.Parse(pis.PropertyType, data[1]);
                    pis.SetValue(model , values);
                }
                else
                {
                    object value = Convert.ChangeType(data[1], pis.PropertyType);
                    pis.SetValue(model, value);
                }
            }
        }

        private void ReadFill(string line, T model)
        {
            string[] data = line.Split(':');
            if (data.Length < 2)
            {
                return;
            }
            OnReadFill(data, model);
        }

        public T FiersOrDefault(Func<T, bool> predicate) =>
           _list.FirstOrDefault(predicate);

        public bool Any(Func<T, bool> predicate) =>
            _list.Any(predicate);

    }
}
