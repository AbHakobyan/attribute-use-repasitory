﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attribute_Use_Repasitory.Repazitory
{
    interface IBase_Repasitory<T> where T : class,new()
    {
        int SaveChanges();
        IEnumerable<T> AsEnumerable();
        void Add(T model);
        void AddRange(IEnumerable<T> model);
        void Insert(int index, T model);
    }
}
